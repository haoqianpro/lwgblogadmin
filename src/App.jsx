import './App.css';
import { Link, Route, Switch, useHistory, useLocation } from 'react-router-dom'
import ArticleList from './components/ArticleList'
import Create from './components/Create'
import Update from './components/Update'
function App() {
  
  const history = useHistory()
  const location = useLocation()
  const handleClick = () => {
    history.push("/")
  }
  return (
    <div className="App">
      <header className="header">
        <h1 onClick={ () => handleClick()} style={{cursor:"pointer"}}>李梧歌的博客</h1>
        {
          location.pathname === "/" ?
          (<Link to="/create">新建文章</Link>):
          (<Link to="/">文章列表</Link>)
        }
      </header>
      <Switch>
        <Route exact path="/">
          <ArticleList />
        </Route>
        <Route path="/create">
          <Create />
        </Route>
        <Route path="/update/:id">
          <Update />
        </Route>
      </Switch>
    </div>
  );
}

export default App;
