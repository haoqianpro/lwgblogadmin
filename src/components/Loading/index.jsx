import styles from './Loading.module.css'

const Loading = () => {
  return (
    <div className={styles.loading}>
      <div className={styles.circle}></div>
      <div>数据加载中</div>
    </div>
  )
}

export default Loading